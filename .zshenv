export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"

export ZDOTDIR="$XDG_CONFIG_HOME/zsh"

export PROJECTS_DIR="$HOME/Developer"
export BROWSER=firefox
export EDITOR=nvim

if [ -f "$XDG_CONFIG_HOME/gitlab-token" ]; then
  export CI_JOB_TOKEN=$(cat "$XDG_CONFIG_HOME/gitlab-token")
fi

source "$ZDOTDIR/zpath"
source "$HOME/.cargo/env"

export HOMEBREW_PREFIX="/opt/homebrew"
export HOMEBREW_CELLAR="/opt/homebrew/Cellar"
export HOMEBREW_REPOSITORY="/opt/homebrew"
export HOMEBREW_AUTO_UPDATE_SECS=3600
export PATH="/opt/homebrew/bin:/opt/homebrew/sbin${PATH+:$PATH}"
export MANPATH="/opt/homebrew/share/man${MANPATH+:$MANPATH}:"
export INFOPATH="/opt/homebrew/share/info:${INFOPATH:-}"
export DOTNET_ROOT="/opt/homebrew/opt/dotnet/libexec"
export LOCALSTACK_ENDPOINT_URL="http://localhost:4566"
export BUN_INSTALL="${HOME}/.local/share" 
export DOTNET_CLI_TELEMETRY_OPTOUT=1
export SSH_AUTH_SOCK=$HOME/.bitwarden-ssh-agent.sock
export PODMAN_COMPOSE_WARNING_LOGS=0

fpath[1,0]="/opt/homebrew/share/zsh/site-functions"
