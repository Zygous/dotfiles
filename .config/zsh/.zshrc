setopt appendhistory autocd extendedglob notify

bindkey -v
export KEYTIMEOUT=1

fpath+="$ZDOTDIR/fns"
fpath+="$ZDOTDIR/pure"

if type brew &>/dev/null; then
  fpath+=$(brew --prefix)/share/zsh/site-functions
  fpath+=$(brew --prefix)/share/zsh-completions
  source $(brew --prefix)/share/zsh-autosuggestions/zsh-autosuggestions.zsh
fi

zstyle :compinstall filename "$ZDOTDIR/.zshrc"

CASE_SENSITIVE="true"
export EDITOR='nvim'

autoload -Uz compinit
compinit

source "$ZDOTDIR/zpath"
source "$ZDOTDIR/aliases"

eval "$(atuin init zsh)"

autoload -U promptinit; promptinit
prompt pure
