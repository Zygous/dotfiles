local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable",
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

vim.g.mapleader = ";"
vim.g.maplocalleader = "\\"

vim.o.termguicolors = true

vim.o.splitright = true
vim.o.splitbelow = true

vim.o.expandtab = true
vim.o.shiftwidth = 2
vim.o.number = true
vim.o.cursorline = true
vim.o.wrap = false
vim.o.colorcolumn = "80"
vim.o.signcolumn = "yes"

-- Configure properly highlighting code fences returned by Deno LS
vim.g.markdown_fenced_languages = {
  "ts=typescript",
}

require("lazy").setup({
  {
    "folke/snacks.nvim",
    priority = 1000,
    lazy = false,
    ---@type snacks.Config
    opts = {
      bigfile = { enabled = true },
      dashboard = { enabled = false },
      indent = { enabled = true },
      input = { enabled = true },
      picker = { enabled = true },
      notifier = { enabled = true },
      quickfile = { enabled = true },
      scroll = { enabled = false },
      statuscolumn = { enabled = true },
      words = { enabled = true },
    },
    keys = {
      {"<leader>gB", function () Snacks.gitbrowse() end, desc = "Git Browse", mode = { "n", "v" } },
      { "<leader>gg", function() Snacks.lazygit() end, desc = "Lazygit" },
      { "<c-/>",      function() Snacks.terminal() end, desc = "Toggle Terminal" },
      { "<leader>bo", function() Snacks.bufdelete.other() end, desc = "Delete all other buffers" },
    }
  },
  "duane9/nvim-rg",
  {
    "folke/which-key.nvim",
    event = "VeryLazy",
    opts = {
      -- your configuration comes here
      -- or leave it empty to use the default settings
      -- refer to the configuration section below
    },
    keys = {
      {
        "<leader>?",
        function()
          require("which-key").show({ global = false })
        end,
        desc = "Buffer Local Keymaps (which-key)",
      },
    },
  },
  "mustache/vim-mustache-handlebars",
  { 'echasnovski/mini.icons', version = false, config = function() require("mini.icons").setup{} end },
  {
    'mrcjkb/rustaceanvim',
    version = '^5',
    lazy = false, -- This plugin is already lazy
  },
  {
    "pappasam/nvim-repl",
    init = function()
      vim.g["repl_filetype_commands"] = {
        bash = "bash",
        janet = "janet -e '(import spork/netrepl) (netrepl/server)'",
        javascript = "node",
        lisp = "ros run --eval '(ql:quickload :swank)'  --eval '(swank:create-server :dont-close t)'",
        vim = "nvim --clean -ERM",
        zsh = "zsh",
      }
    end,
    keys = {
      { "<Leader>rc", "<Plug>(ReplSendCell)", mode = "n", desc = "Send Repl Cell" },
      { "<Leader>rr", "<Plug>(ReplSendLine)", mode = "n", desc = "Send Repl Line" },
      { "<Leader>r", "<Plug>(ReplSendVisual)", mode = "x", desc = "Send Repl Visual Selection" }
    },
  },
  {
    "pmizio/typescript-tools.nvim",
    dependencies = { "nvim-lua/plenary.nvim", "neovim/nvim-lspconfig" },
    opts = {},
  },
  {
    "clojure-vim/vim-jack-in",
    dependencies = {
      "tpope/vim-dispatch",
      "radenling/vim-dispatch-neovim",
    }
  },
  {
    "EdenEast/nightfox.nvim",
    config = function()
      require("nightfox").setup {
        options = {
          transparent = true
        }
      }
      vim.cmd("colo nightfox")
    end
  },
  {
    "vhyrro/luarocks.nvim",
    priority = 1000,
    config = true,
  },
  "tpope/vim-commentary",
  "tpope/vim-eunuch",
  "tpope/vim-sensible",
  "tpope/vim-repeat",
  "tpope/vim-unimpaired",
  "kovisoft/paredit",
  "guns/vim-sexp",
  "tpope/vim-sexp-mappings-for-regular-people",
  "tpope/vim-dispatch",
  "radenling/vim-dispatch-neovim",
  {
    "nvim-tree/nvim-web-devicons",
    config = function()
      require("nvim-web-devicons").setup()
    end,
  },
  {
    "norcalli/nvim-colorizer.lua",
    config = function()
      require("colorizer").setup()
    end,
  },
  {
    "stevearc/dressing.nvim",
    config = function()
      require("dressing").setup()
    end,
  },
  {
    "saecki/crates.nvim",
    config = function()
      require("crates").setup()
    end,
  },
  {
    "nvim-treesitter/nvim-treesitter",
    build = function()
      vim.cmd([[TSUpdate]])
    end,
  },
  "nvim-treesitter/nvim-treesitter-refactor",
  "nvim-treesitter/nvim-treesitter-textobjects",
  {
    "nvim-telescope/telescope.nvim",
    tag = "0.1.6",
    dependencies = {
      "nvim-lua/plenary.nvim",
      { "nvim-telescope/telescope-live-grep-args.nvim", version = "^1.0.0" }
    },
    -- opts = {
    --   defaults = {
    --     borderchars = {
    --       prompt = { "─", " ", " ", " ", "─", "─", " ", " " },
    --       results = { " " },
    --       preview = { " " },
    --     },
    --   },
    -- },
    config = function()
      local telescope = require("telescope")
      telescope.setup()

      local builtin = require("telescope.builtin")
      vim.keymap.set(
        "n",
        "<leader>ff",
        builtin.find_files,
        { desc = "Find files" }
      )
      vim.keymap.set(
        "n",
        "<leader>fb",
        builtin.buffers,
        { desc = "Find buffer" }
      )
      vim.keymap.set(
        "n",
        "<leader>fg",
        ":lua require('telescope').extensions.live_grep_args.live_grep_args()<CR>",
        { desc = "Live grep" }
      )
      vim.keymap.set(
        "n",
        "<leader>fr",
        builtin.lsp_references,
        { desc = "Find references" }
      )

      telescope.load_extension("live_grep_args")
    end,
  },
  {
    "iamcco/markdown-preview.nvim",
    cmd = { "MarkdownPreviewToggle", "MarkdownPreview", "MarkdownPreviewStop" },
    ft = { "markdown" },
    build = function() vim.fn["mkdp#util#install"]() end,
  },
  {
    "kylechui/nvim-surround",
    version = "*", -- Use for stability; omit to use `main` branch for the latest features
    event = "VeryLazy",
    config = function()
      require("nvim-surround").setup{}
    end
  },
  {
    "echasnovski/mini.jump2d",
    version = false,
    lazy = false,
    config = function()
      require("mini.jump2d").setup({
        mappings = { start_jumping = '<leader>j' }
      })
    end,
  },
  {
    "echasnovski/mini.indentscope",
    version = false,
    config = function()
      require("mini.indentscope").setup({ symbol = "❘" })
    end,
  },
  {
    "echasnovski/mini.sessions",
    version = false,
    config = function()
      require("mini.sessions").setup({
        autoread = true,
        directory = "",
        file = "Session.vim",
      })
    end,
  },
  {
    "mfussenegger/nvim-lint",
    config = function()
      require("lint").linters_by_ft = {
        -- lua = { "selene" },
      }
      vim.api.nvim_create_autocmd("BufWritePost", {
        callback = function()
          require("lint").try_lint()
        end,
      })
    end,
  },
  {
    "stevearc/conform.nvim",
    config = function()
      require("conform").setup({
        formatters_by_ft = {
          clojure = { "joker" },
          javascript = { "prettierd" },
          json = { "prettierd" },
          html = { "prettierd" },
          lua = { "stylua" },
          rust = { "rustfmt" },
          typescript = { "prettierd" },
        },
      })

      vim.keymap.set("n", "<leader>fc", function()
        require("conform").format({ lsp_fallback = true })
      end)
    end,
  },

  -- Git
  "tpope/vim-fugitive",
  {
    "lewis6991/gitsigns.nvim",
    config = function ()
      require("gitsigns").setup({
        signs = {
          add          = { text = '┃' },
          change       = { text = '⌇' },
          delete       = { text = '_' },
          topdelete    = { text = '‾' },
          changedelete = { text = '󰆴' },
          untracked    = { text = '' },
        },
        signcolumn = true,
        numhl      = true,
        word_diff  = false, -- Toggle with `:Gitsigns toggle_word_diff`
        watch_gitdir = {
          follow_files = true
        },
        auto_attach = true,
        attach_to_untracked = false,
        current_line_blame = false, -- Toggle with `:Gitsigns toggle_current_line_blame`
        current_line_blame_opts = {
          virt_text = true,
          virt_text_pos = 'overlay', -- 'eol' | 'overlay' | 'right_align'
          delay = 1000,
          ignore_whitespace = false,
          virt_text_priority = 100,
        },
        current_line_blame_formatter = '<author>, <author_time:%Y-%m-%d> - <summary>',
        sign_priority = 6,
        update_debounce = 100,
        status_formatter = nil, -- Use default
        max_file_length = 40000, -- Disable if file is longer than this (in lines)
        preview_config = {
          -- Options passed to nvim_open_win
          border = 'single',
          style = 'minimal',
          relative = 'cursor',
          row = 0,
          col = 1
        },
        on_attach = function(bufnr)
          local gitsigns = require('gitsigns')

          local function map(mode, l, r, opts)
            opts = opts or {}
            opts.buffer = bufnr
            vim.keymap.set(mode, l, r, opts)
          end

          -- Navigation
          map('n', ']c', function()
            if vim.wo.diff then
              vim.cmd.normal({']c', bang = true})
            else
              gitsigns.nav_hunk('next')
            end
          end)

          map('n', '[c', function()
            if vim.wo.diff then
              vim.cmd.normal({'[c', bang = true})
            else
              gitsigns.nav_hunk('prev')
            end
          end)

          -- Actions
          map('n', '<leader>hs', gitsigns.stage_hunk)
          map('n', '<leader>hr', gitsigns.reset_hunk)
          map('v', '<leader>hs', function() gitsigns.stage_hunk {vim.fn.line('.'), vim.fn.line('v')} end)
          map('v', '<leader>hr', function() gitsigns.reset_hunk {vim.fn.line('.'), vim.fn.line('v')} end)
          map('n', '<leader>hS', gitsigns.undo_stage_hunk)
          map('n', '<leader>hR', gitsigns.reset_buffer)
          map('n', '<leader>hp', gitsigns.preview_hunk)
          map('n', '<leader>hb', function() gitsigns.blame_line{full=true} end)
          map('n', '<leader>tb', gitsigns.toggle_current_line_blame)
          map('n', '<leader>hd', gitsigns.diffthis)
          map('n', '<leader>hD', function() gitsigns.diffthis('~') end)
          map('n', '<leader>td', gitsigns.toggle_deleted)

          -- Text object
          map({'o', 'x'}, 'ih', ':<C-U>Gitsigns select_hunk<CR>')
        end
      })

      --- Push, and create a merge request
      vim.keymap.set(
        "n",
        "<leader>gp",
        "<cmd>!git push -u origin HEAD -o merge_request.create -o merge_request.remove_source_branch<cr>"
      )
    end
  },

  -- Language Support
  -- Crates uses none-ls
  {
    "nvimtools/none-ls.nvim",
    config = function()
      require("null-ls").setup()
    end,
  },
  "windwp/nvim-ts-autotag",
  {
    "neovim/nvim-lspconfig",
    lazy = false,
    dependencies = {
      {"ms-jpq/coq_nvim", branch = "coq"},
      {"ms-jpq/coq.artifacts", branch = "artifacts"},
    },
    init = function()
      vim.g.coq_settings = {
          auto_start = true
      }

      -- Keybindings
      vim.api.nvim_set_keymap('i', '<Esc>', [[pumvisible() ? "\<C-e><Esc>" : "\<Esc>"]], { expr = true, silent = true })
      vim.api.nvim_set_keymap('i', '<C-c>', [[pumvisible() ? "\<C-e><C-c>" : "\<C-c>"]], { expr = true, silent = true })
      vim.api.nvim_set_keymap('i', '<BS>', [[pumvisible() ? "\<C-e><BS>" : "\<BS>"]], { expr = true, silent = true })
      vim.api.nvim_set_keymap(
        "i",
        "<CR>",
        [[pumvisible() ? (complete_info().selected == -1 ? "\<C-e><CR>" : "\<C-y>") : "\<CR>"]],
        { expr = true, silent = true }
      )
      end,
      config = function()
      require("lspconfig").gleam.setup{}

      vim.api.nvim_create_autocmd("LspAttach", {
        group = vim.api.nvim_create_augroup("UserLspConfig", {}),
        callback = function(ev)
          vim.keymap.set("n", "<leader>e", vim.diagnostic.open_float)
          vim.keymap.set("n", "gd", vim.lsp.buf.definition, { buffer = ev.buf, desc = "Go to definition" })
          vim.keymap.set("n", "gr", vim.lsp.buf.references, { buffer = ev.buf, desc = "Show references" })
          vim.keymap.set("n", "gI", vim.lsp.buf.implementation, { buffer = ev.buf, desc = "Go to implementation" })
          vim.keymap.set("n", "<leader>rn", vim.lsp.buf.rename, { buffer = ev.buf, desc = "Rename" })
          vim.keymap.set({ "n", "v" }, "<leader>ca", vim.lsp.buf.code_action, { buffer = ev.buf, desc = "Code actions..."})
        end,
      })
    end,
  },
  "williamboman/mason.nvim",
  "williamboman/mason-lspconfig.nvim",
  {
    "WhoIsSethDaniel/mason-tool-installer",
    config = function()
      require("mason").setup()
      require("mason-tool-installer").setup({
        auto_update = true,
        ensure_installed = {
          -- "clj-kondo",
          "eslint-lsp",
          "html-lsp",
          "prettierd",
          "taplo",
        },
      })
      require("mason-lspconfig").setup({
        automatic_installation = true,
        handlers = {
          function(server_name)
            require("lspconfig")[server_name].setup{}
          end,

          ["intelephense"] = function ()
                local f = assert(io.open(os.getenv("HOME") .. "/.config/intelephense", "rb"))
                local content = f:read("*a")
                f:close()
                local licence = string.gsub(content, "%s+", "")
                require("lspconfig").intelephense.setup {
                  capabilities = capabilities,
                  on_attach = on_attach,
                  init_options = {
                    licenceKey = licence
                  }
                }
          end,

          ["html"] = function()
            require("lspconfig")["html"].setup{}
          end,
        },
      })
    end,
  },
})

require("nvim-treesitter.configs").setup({
  ensure_installed = {
    "clojure",
    "comment",
    "diff",
    "dockerfile",
    "gleam",
    "html",
    "javascript",
    "json",
    "lua",
    "markdown",
    "markdown_inline",
    "python",
    "rust",
    "swift",
    "terraform",
    "toml",
    "typescript",
    "vimdoc",
    "yaml",
  },
  highlight = { enable = true },
  indent = { enable = true },
  autotag = { enable = true },
  textobjects = {
    select = {
      enable = true,

      -- Automatically jump forward to textobj, similar to targets.vim
      lookahead = true,

      keymaps = {
        -- You can use the capture groups defined in textobjects.scm
        ["af"] = "@function.outer",
        ["if"] = "@function.inner",
        ["ac"] = "@class.outer",
        -- You can optionally set descriptions to the mappings (used in the desc parameter of
        -- nvim_buf_set_keymap) which plugins like which-key display
        ["ic"] = {
          query = "@class.inner",
          desc = "Select inner part of a class region",
        },
        -- You can also use captures from other query groups like `locals.scm`
        ["as"] = {
          query = "@scope",
          query_group = "locals",
          desc = "Select language scope",
        },
      },
      -- You can choose the select mode (default is charwise 'v')
      --
      -- Can also be a function which gets passed a table with the keys
      -- * query_string: eg '@function.inner'
      -- * method: eg 'v' or 'o'
      -- and should return the mode ('v', 'V', or '<c-v>') or a table
      -- mapping query_strings to modes.
      selection_modes = {
        ["@parameter.outer"] = "v", -- charwise
        ["@function.outer"] = "V", -- linewise
        ["@class.outer"] = "<c-v>", -- blockwise
      },
      -- If you set this to `true` (default is `false`) then any textobject is
      -- extended to include preceding or succeeding whitespace. Succeeding
      -- whitespace has priority in order to act similarly to eg the built-in
      -- `ap`.
      --
      -- Can also be a function which gets passed a table with the keys
      -- * query_string: eg '@function.inner'
      -- * selection_mode: eg 'v'
      -- and should return true or false
      include_surrounding_whitespace = true,
    },
  },
})

local diagnostics_signs = {
  Error = "",
  Warn = "",
  Hint = "💡",
  Info = "",
}
for type, icon in pairs(diagnostics_signs) do
  local hl = "DiagnosticSign" .. type
  vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = hl })
end

-- Terminal stuff
vim.api.nvim_create_user_command(
  "Term",
  ":sp|term",
  { desc = "Open a terminal in a horizontal split", bang = true }
)
vim.api.nvim_create_user_command(
  "VTerm",
  ":vsp|term",
  { desc = "Open a terminal in a vertical split", bang = true }
)
vim.api.nvim_create_autocmd({ "TermOpen" }, { command = "set nonumber" })
---  Easier back to Normal mode in a terminal window
vim.keymap.set("t", "<leader>`", "<c-\\><c-n>")
